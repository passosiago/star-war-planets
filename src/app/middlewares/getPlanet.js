const axios = require("axios");
const { Cache } = require("memory-cache");
const myCache = new Cache();
const TTL = process.env.TTL || 300;
let PLANETS_ENDPOINT =
    process.env.PLANETS_ENDPOINT || "https://swapi.co/api/planets";

async function makeRequest(req, res, next, url) {
    let { name } = req.body;
    let hasPlanet = false;
    let planets;
    let result;

    result = myCache.get(url);
    if (result) {
        planets = result.data.results;
        handlePlanets(planets);
    }
    result = await axios.get(url);
    myCache.put(url, result, TTL);
    planets = result.data.results;
    handlePlanets(planets);

    function handlePlanets(planets) {
        planets.forEach(planet => {
            if (planet.name === name) {
                req.body.movieApperanceCount = planet.films.length;
                req.body.movieApperance = [...planet.films];
                hasPlanet = true;
                return next();
            }
        });
        if (!hasPlanet) {
            if (result.data.next != null) {
                makeRequest(req, res, next, result.data.next);
            }

            if (result.data.next === null)
                return res.status(400).json({
                    error: "Only planets registered on Swapi api can be stored."
                });
        }
    }
}

module.exports = (req, res, next) => {
    makeRequest(req, res, next, "https://swapi.co/api/planets");
};
