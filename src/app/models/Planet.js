const mongoose = require("mongoose");

const PlanetSchema = new mongoose.Schema({
    name: {
        type: String,
        required: true
    },
    climate: {
        type: String,
        required: true
    },
    terrain: {
        type: String,
        required: true
    },
    movieApperanceCount: {
        type: Number,
        default: 0
    },
    movieApperance: [
        {
            type: String
        }
    ],
    createdAt: {
        type: Date,
        default: Date.now
    }
});

module.exports = mongoose.model("Planet", PlanetSchema);
