const Planet = require("../models/Planet");

class PlanetController {
    async index(req, res) {
        try {
            const planets = await Planet.find();
            return res.json(planets);
        } catch (e) {
            return res.status(500).json({ error: "Internal Server Error" });
        }
    }

    async find(req, res) {
        try {
            const planet = await Planet.findById(req.params.id);
            return res.json(planet);
        } catch (e) {
            return res.status(500).json({ error: "Internal Server Error" });
        }
    }

    async findByName(req, res) {
        const planet = await Planet.findOne({ name: req.params.name });
        if (!planet)
            return res.status(404).json({ error: "Planet not found." });
        return res.json(planet);
    }
    async store(req, res) {
        const { name } = req.body;
        if (await Planet.findOne({ name })) {
            return res.status(400).json({ error: "Planet already exists." });
        }
        const planet = await Planet.create(req.body);
        return res.status(201).json(planet);
    }

    async update(req, res) {
        try {
            const planet = await Planet.findByIdAndUpdate(
                req.params.id,
                req.body,
                {
                    new: true
                }
            );
            return res.json(planet);
        } catch (e) {
            res.status(500).json({ error: "Internal Server Error" });
        }
    }

    async destroy(req, res) {
        try {
            const planet = await Planet.findByIdAndDelete(req.params.id);
            return res.send();
        } catch (e) {
            res.status(500).json({ error: "Internal Server Error" });
        }
    }
}

module.exports = new PlanetController();
