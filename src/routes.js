const router = require("express").Router();
const PlanetController = require("./app/controllers/PlanetController");
const fetchPlanets = require("./app/middlewares/getPlanet");

router.get("/planets", PlanetController.index);
router.get("/planets/:id", PlanetController.find);
router.get("/planets/name/:name", PlanetController.findByName);
router.post("/planets", fetchPlanets, PlanetController.store);
router.put("/planets/:id", PlanetController.update);
router.delete("/planets/:id", PlanetController.destroy);

module.exports = router;
