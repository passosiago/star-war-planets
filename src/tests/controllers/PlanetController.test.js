const chai = require("chai");
chai.use(require("sinon-chai"));
const { expect } = chai;
const proxyquire = require("proxyquire");
const sinon = require("sinon");
const sandbox = sinon.createSandbox();
const findStub = sandbox.stub();
const findByIdStub = sandbox.stub();
const findOneStub = sandbox.stub();
const createStub = sandbox.stub();
const findByIdAndUpdateStub = sandbox.stub();
const findByIdAndDeleteStub = sandbox.stub();

const PlanetController = proxyquire("../../app/controllers/PlanetController", {
    "../models/Planet": {
        find: findStub,
        findById: findByIdStub,
        findOne: findOneStub,
        create: createStub,
        findByIdAndUpdate: findByIdAndUpdateStub,
        findByIdAndDelete: findByIdAndDeleteStub
    }
});

let fakePlanet = {
    name: "theName",
    terrain: "theTerrain",
    climate: "theClimate"
};
describe("PlanetController", () => {
    afterEach(() => sandbox.reset());
    describe("index", () => {
        it("Should return all planets", done => {
            findStub.returns(fakePlanet);
            let req = {};
            let res = {
                json: result => {
                    expect(result).to.be.deep.equals(fakePlanet);
                    done();
                }
            };

            PlanetController.index(req, res);
        });
        it("Should return status 500 and json with Internal Server Error, if something unexpected happened", done => {
            findStub.throwsException();
            let req = {};
            let res = {
                status: status => {
                    return {
                        json: result => {
                            expect(status).to.be.equal(500);
                            expect(result).to.be.deep.equals({
                                error: "Internal Server Error"
                            });
                            done();
                        }
                    };
                }
            };
            PlanetController.index(req, res);
        });
    });
    describe("find", () => {
        it("Should return the planet correspondent to id passed", done => {
            findByIdStub.returns({ planet: "thePlanet" });
            let req = {
                params: {
                    id: "theId"
                }
            };
            let res = {
                json: result => {
                    expect(findByIdStub).to.be.calledWith("theId");
                    expect(result).to.be.deep.equals({ planet: "thePlanet" });
                    done();
                }
            };
            PlanetController.find(req, res);
        });
        it("Should return status 500 and json with Internal Server Error, if something unexpected happened", done => {
            findByIdStub.throwsException();
            let req = {
                params: {
                    id: "theId"
                }
            };
            let res = {
                status: status => {
                    return {
                        json: result => {
                            expect(findByIdStub).to.be.calledWith("theId");
                            expect(status).to.be.equal(500);
                            expect(result).to.be.deep.equals({
                                error: "Internal Server Error"
                            });
                            done();
                        }
                    };
                }
            };
            PlanetController.find(req, res);
        });
    });

    describe("findByName", () => {
        it("Should return the planet correspondent to name passed", done => {
            findOneStub.returns({ planet: "thePlanet" });
            let req = {
                params: {
                    name: "theName"
                }
            };
            let res = {
                json: result => {
                    expect(findOneStub).to.be.calledWith({ name: "theName" });
                    expect(result).to.be.deep.equals({ planet: "thePlanet" });
                    done();
                }
            };
            PlanetController.findByName(req, res);
        });
        it("Should return status 404 and json with 'Planet not found.', if planet does not exist in db", done => {
            findByIdStub.throwsException();
            let req = {
                params: {
                    name: "theName"
                }
            };
            let res = {
                status: status => {
                    return {
                        json: result => {
                            expect(findOneStub).to.be.calledWith({
                                name: "theName"
                            });
                            expect(status).to.be.equal(404);
                            expect(result).to.be.deep.equals({
                                error: "Planet not found."
                            });
                            done();
                        }
                    };
                }
            };
            PlanetController.findByName(req, res);
        });
    });
    describe("store", () => {
        it("Should create the planet and status 201", done => {
            findOneStub.returns(undefined);
            createStub.returns(fakePlanet);
            let req = {
                body: fakePlanet
            };
            let res = {
                status: status => {
                    return {
                        json: result => {
                            expect(findOneStub).to.be.calledWith({
                                name: "theName"
                            });
                            expect(createStub).to.be.calledWith(fakePlanet);
                            expect(result).to.be.deep.equals(fakePlanet);
                            expect(status).to.be.equal(201);
                            done();
                        }
                    };
                }
            };
            PlanetController.store(req, res);
        });
        it("Should return status 400 and json with 'Planet already exists.', if planet already exist in db", done => {
            findOneStub.returns(fakePlanet);
            let req = {
                body: fakePlanet
            };
            let res = {
                status: status => {
                    return {
                        json: result => {
                            expect(findOneStub).to.be.calledWith({
                                name: fakePlanet.name
                            });
                            expect(status).to.be.equal(400);
                            expect(result).to.be.deep.equals({
                                error: "Planet already exists."
                            });
                            done();
                        }
                    };
                }
            };
            PlanetController.store(req, res);
        });
    });

    describe("update", () => {
        it("Should update the planet and return it", done => {
            findByIdAndUpdateStub.returns(fakePlanet);
            createStub.returns(fakePlanet);
            let req = {
                body: {
                    fakePlanet
                },
                params: {
                    id: "theId"
                }
            };
            let res = {
                json: result => {
                    expect(findByIdAndUpdateStub).to.be.calledWith(
                        "theId",
                        { fakePlanet },
                        { new: true }
                    );
                    expect(result).to.be.deep.equals(fakePlanet);
                    done();
                }
            };
            PlanetController.update(req, res);
        });
        it("Should return status 500 and json with 'Internal Server Error', if planet is not on db", done => {
            findByIdAndUpdateStub.throwsException();
            let req = {
                body: {
                    fakePlanet
                },
                params: {
                    id: "theId"
                }
            };
            let res = {
                status: status => {
                    return {
                        json: result => {
                            expect(status).to.be.equal(500);
                            expect(result).to.be.deep.equals({
                                error: "Internal Server Error"
                            });
                            done();
                        }
                    };
                }
            };
            PlanetController.update(req, res);
        });
    });
    describe("destroy", () => {
        it("Should delete the planet", done => {
            findByIdAndDeleteStub.returns(fakePlanet);
            let req = {
                params: {
                    id: "theId"
                }
            };
            let res = {
                send: send => {
                    expect(findByIdAndDeleteStub).to.be.calledWith("theId");
                    done();
                }
            };
            PlanetController.destroy(req, res);
        });
        it("Should return status 500 and json with 'Internal Server Error', if something unexpect happen", done => {
            findByIdAndDeleteStub.throwsException();
            let req = {
                params: {
                    id: "theId"
                }
            };
            let res = {
                status: status => {
                    return {
                        json: result => {
                            expect(findByIdAndDeleteStub).to.be.calledWith(
                                "theId"
                            );
                            expect(status).to.be.equal(500);
                            expect(result).to.be.deep.equals({
                                error: "Internal Server Error"
                            });
                            done();
                        }
                    };
                }
            };
            PlanetController.destroy(req, res);
        });
    });
});
