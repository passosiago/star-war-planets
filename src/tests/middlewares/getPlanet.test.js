const chai = require("chai");
chai.use(require("sinon-chai"));
const { expect } = chai;
const sinon = require("sinon");
const proxyquire = require("proxyquire");
const sandbox = sinon.createSandbox();

const getStub = sandbox.stub();
const cacheGetStub = sandbox.stub();
const cachePutStub = sandbox.stub();

const fetchPlanets = proxyquire("../../app/middlewares/getPlanet", {
    axios: {
        get: getStub
    },
    "memory-cache": {
        Cache: function() {
            this.get = cacheGetStub;
            this.put = cachePutStub;
        }
    }
});

let successfulData = {
    data: {
        next: "nextUrl",
        results: [{ name: "fakePlanet", films: ["fakeMovie"] }]
    }
};
let successfulDataPage2 = {
    data: {
        next: null,
        results: [{ name: "fakePlanet2", films: ["fakeMovie2"] }]
    }
};
describe("getPlanetService", () => {
    afterEach(() => {
        sandbox.reset();
    });
    describe("fetchPlanets", () => {
        it("Should make request and put to cache", done => {
            cacheGetStub.returns(null);
            getStub.returns(successfulData);
            let req = {
                body: {
                    name: "fakePlanet"
                },
                url: "localhost/planets"
            };
            let res = {};
            fetchPlanets(req, res, () => {
                expect(req.body.movieApperanceCount).to.be.equal(1);
                expect(getStub).to.be.calledWith(
                    "https://swapi.co/api/planets"
                );
                done();
            });
        });
        it("Should retrive data from cache", done => {
            cacheGetStub.returns(successfulData);
            let req = {
                body: {
                    name: "fakePlanet"
                },
                url: "localhost/planets"
            };
            let res = {};
            fetchPlanets(req, res, () => {
                expect(cacheGetStub).to.be.calledWith(
                    "https://swapi.co/api/planets"
                );
                expect(req.body.movieApperanceCount).to.be.equal(1);
                done();
            });
        });
    });
});
