const express = require("express");
const mongoose = require("mongoose");
const databaseConfig = require("./config/database");

class App {
    constructor() {
        this.express = express();
        this.database();
        this.midlewares();
        this.routes();
    }

    database() {
        mongoose.connect(databaseConfig.uri, {
            useCreateIndex: true,
            useNewUrlParser: true
        });
    }

    midlewares() {
        this.express.use(express.json());
        this.express.use(express.urlencoded({ extended: false }));
    }

    routes() {
        this.express.use("/api", require("./routes"));
    }
}

module.exports = new App().express;
