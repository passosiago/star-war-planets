# Desafio B2W

## Depêndencias para o projeto

-   [NodeJs](https://nodejs.org/en/)
-   [Npm](https://www.npmjs.com/)
-   [Docker](https://docs.docker.com/install/overview/)
-   [Docker-Compose](https://docs.docker.com/compose/install/)

## Inicialização do Projeto

```bash
$ docker-compose up --build
```

## Execução de teste 

```bash
$ npm run test
```

## Execução de cobertura de teste

```bash
$ npm run coverage
```
## Endpoints 

|           Url           | Method |    Action     |
| :---------------------: | :----: | :-----------: |
|      /api/planets       |  GET   | List Planets  |
|    /api/planets/:id     |  GET   | Return Planet |
| /api/planets/name/:name |  GET   | Return Planet |
|      /api/planets       |  POST  | Store Planet  |
|    /api/planets/:id     |  PUT   | Update Planet |
|    /api/planets/:id     | DELETE | Remove Planet |

## Estrutura dos dados

# Criar planeta - POST - localhost:3000/api/planets

```json
{
    "name": "Alderann", 
    "climate": "temperate, tropical",
    "terrain": "jungle, rainforests"
}
```
# Listar Planetas - GET - localhost:3000/api/planets

```json
{
  "movieApperanceCount": 2,
  "movieApperance": [
    "https://swapi.co/api/films/6/",
    "https://swapi.co/api/films/1/"
  ],
  "_id": "5c7da13a53af6f26842e1974",
  "name": "Alderaan",
  "climate": "temperate, tropical",
  "terrain": "jungle, rainforests",
  "createdAt": "2019-03-04T22:05:46.035Z",
  "__v": 0
}

```
