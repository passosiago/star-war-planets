FROM node:8.9.3-alpine

EXPOSE 3000

ADD . /star-war-planets

RUN cd /star-war-planets && npm install 

VOLUME /star-war-planets/node_modules

WORKDIR /star-war-planets

CMD ["npm", "start"]
